﻿namespace VeeamFileSignatureGenerator.Abstractions
{
    public interface IHashCalculator
    {
        void CalculateHash();
    }
}
