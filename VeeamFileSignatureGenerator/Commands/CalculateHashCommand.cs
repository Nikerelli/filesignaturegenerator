﻿using System;
using System.IO;

namespace VeeamFileSignatureGenerator.Commands
{
    public class CalculateHashCommand
    {
        public long BlockSize { get; private set; }
        public string FilePath { get; private set; }

        private CalculateHashCommand(string filePath, long blockSize)
        {
            BlockSize = blockSize;
            FilePath = filePath;
        }

        public static CalculateHashCommand Parse(string[] args)
        {
            try
            {
                if (args == null)
                {
                    throw new ArgumentException($"Input string cannot be null or empty");
                }

                if (args.Length != 2)
                {
                    throw new ArgumentException("Command has less or more than 3 keywords");
                }

                var inputFilePath = args[0];
                if (inputFilePath.IndexOfAny(Path.GetInvalidPathChars()) != -1)
                    throw new ArgumentException($"Invalid input file parameter: {inputFilePath}. It must be correct file path.");
                if (!File.Exists(inputFilePath))
                    throw new ArgumentException($"Input file with path {inputFilePath} not exists.");
                var fileInfo = new FileInfo(inputFilePath);
                if (fileInfo.Length == 0)
                    throw new ArgumentException($"Source file {inputFilePath} is empty.");

                return new CalculateHashCommand(args[0], long.Parse(args[1]));
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message, ex);
            }
        }
    }
}
