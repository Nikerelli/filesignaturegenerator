﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace VeeamFileSignatureGenerator.SharedIoHashGenerator.Infrastructure
{
    public abstract class ConcurrentObjPool<TModel>
        where TModel : class, IDisposable
    {
        protected readonly TModel[] _createdObjects;
        protected readonly ConcurrentQueue<TModel> _freeObjects;

        private readonly Func<TModel> _objectFactory;
        private readonly Semaphore _accessLocker;

        public ConcurrentObjPool(Func<TModel> objectFactory, int initCount = 1)
        {
            if(initCount < 1)
            {
                throw new ArgumentException($"{nameof(ConcurrentObjPool<TModel>)} has {nameof(initCount)} {initCount}");
            }

            if(objectFactory == null)
            {
                throw new ArgumentException($"{nameof(ConcurrentObjPool<TModel>)} {nameof(objectFactory)} is null");
            }
            _objectFactory = objectFactory;

            _freeObjects = new ConcurrentQueue<TModel>();
            _createdObjects = new TModel[initCount];

            for(int i = 0; i < initCount; i++)
            {
                var obj = _objectFactory.Invoke();
                _createdObjects[i] = obj;
                _freeObjects.Enqueue(obj);
            }

            _accessLocker = new Semaphore(initCount, initCount);
        }

        protected TOutput RunOperation<TOutput>(Func<TModel, TOutput> action)
            where TOutput : class
        {
            _accessLocker?.WaitOne();
            _freeObjects.TryDequeue(out var obj);
            try
            {
                return action?.Invoke(obj);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Message: {ex.Message}\nStackTrace: {ex.StackTrace}");
                throw ex;
            }
            finally
            {
                _freeObjects.Enqueue(obj);
                _accessLocker?.Release();
            }
        }

        public void Dispose()
        {
            try
            {
                foreach (var obj in _createdObjects)
                {
                    obj?.Dispose();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Message: {ex.Message}\nStackTrace: {ex.StackTrace}");
            }
        }
    }
}
