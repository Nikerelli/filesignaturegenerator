﻿using System;
using System.IO;

namespace VeeamFileSignatureGenerator.SharedIoHashGenerator.Infrastructure
{
    public class FileStreamPool: ConcurrentObjPool<FileStream>
    {
        public FileStreamPool(string filePath, int initCount) 
            : base(() => new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read), initCount)
        {
        }

        public byte[] ReadBlock(long offset, long count)
        {
            return RunOperation((fs) => Read(fs, offset, count));
        }

        private byte[] Read(FileStream fs, long offset, long count)
        {
            if (fs.Position == fs.Length - 1)
            {
                return new byte[0];
            }

            if (fs.Length < offset)
            {
                throw new ArgumentException($"Given offset {offset} is older than file length {fs.Length}");
            }

            if (fs.Length < offset + count)
            {
                throw new ArgumentException($"Given file end is at {offset + count} is older than file length {fs.Length}");
            }

            fs.Seek(offset, SeekOrigin.Begin);
            var buffer = new byte[count];

            fs.Read(buffer, 0, buffer.Length);

            return buffer;
        }
    }
}
