﻿using System.Security.Cryptography;

namespace VeeamFileSignatureGenerator.SharedIoHashGenerator.Infrastructure
{
    public class Sha256Pool : ConcurrentObjPool<SHA256>
    {
        public Sha256Pool(int initCount)
            : base(SHA256.Create, initCount)
        {

        }

        public byte[] ComputeHash(byte[] segment)
        {
            return RunOperation((hashComputer) => hashComputer.ComputeHash(segment));
        }
    }
}
