﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Text;
using VeeamFileSignatureGenerator.Abstractions;
using VeeamFileSignatureGenerator.Commands;
using VeeamFileSignatureGenerator.Models;
using VeeamFileSignatureGenerator.SharedIoHashGenerator.Infrastructure;
using VeeamFileSignatureGenerator.Utils;

namespace VeeamFileSignatureGenerator.SharedIoHashGenerator
{
    /// <summary>
    ///     Runs X threads with Y IO threads
    ///     Were Y IO threads sharing between X threads
    /// </summary>
    public class SharedHashCalculator : IDisposable, IHashCalculator
    {
        private readonly FileStreamPool _fileAccessor;
        private readonly Sha256Pool _hashGenerator;

        private ConcurrentQueue<FileMarker> _blockQueue;

        private const int StringHashSize = 64;

        public SharedHashCalculator(CalculateHashCommand command)
        {
            var fileInfo = new FileInfo(command.FilePath);
            var blocksTotal = fileInfo.Length / command.BlockSize + (fileInfo.Length % command.BlockSize > 0 ? 1 : 0);
            var markers = new FileMarker[(int)blocksTotal];

            long offset = 0;
            for(int i = 0; i < blocksTotal; i++)
            {
                var blockSize = fileInfo.Length > offset + command.BlockSize
                    ? command.BlockSize
                    : offset + command.BlockSize - fileInfo.Length + 1;
                markers[i] = (new FileMarker(i, offset, blockSize));

                offset += blockSize;
            }

            _blockQueue = new ConcurrentQueue<FileMarker>(markers);
            _fileAccessor = new FileStreamPool(command.FilePath, SystemAnalyzer.GetProcessorsCount());
            _hashGenerator = new Sha256Pool(SystemAnalyzer.GetProcessorsCount());
        }

        public void CalculateHash()
        {
            while (_blockQueue.TryDequeue(out var fileMarker))
            {
                var block = _fileAccessor.ReadBlock(fileMarker.Offset, fileMarker.Count);

                if (block.Length < 1)
                {
                    return;
                }

                var hash = _hashGenerator.ComputeHash(block);

                var sb = new StringBuilder(StringHashSize);
                foreach (var b in hash)
                {
                    sb.Append(b.ToString("x2"));
                }
                Console.WriteLine($"Block #{fileMarker.Number} has hash = [{sb}]");
            }
        }

        public void Dispose()
        {
            _fileAccessor?.Dispose();
            _hashGenerator?.Dispose();
        }


        ~SharedHashCalculator()
        {
            Dispose();
        }
    }
}
