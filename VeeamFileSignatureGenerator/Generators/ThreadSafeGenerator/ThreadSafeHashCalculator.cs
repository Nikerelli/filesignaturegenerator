﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using VeeamFileSignatureGenerator.Abstractions;
using VeeamFileSignatureGenerator.Commands;
using VeeamFileSignatureGenerator.Models;

namespace VeeamFileSignatureGenerator.ThreadSafeGenerator
{
    /// <summary>
    ///     Runs X isolated from each other thread
    ///     Each thread has it own IO streams
    /// </summary>
    public class ThreadSafeHashCalculator : IHashCalculator
    {
        private readonly CalculateHashCommand _command;

        private ConcurrentQueue<FileMarker> _blockQueue;

        private const int StringHashSize = 64;

        public ThreadSafeHashCalculator(CalculateHashCommand command)
        {
            var fileInfo = new FileInfo(command.FilePath);
            var blocksTotal = fileInfo.Length / command.BlockSize + (fileInfo.Length % command.BlockSize > 0 ? 1 : 0);
            var markers = new FileMarker[(int)blocksTotal];

            long offset = 0;
            for (int i = 0; i < blocksTotal; i++)
            {
                var blockSize = fileInfo.Length > offset + command.BlockSize
                    ? command.BlockSize
                    : offset + command.BlockSize - fileInfo.Length + 1;
                markers[i] = (new FileMarker(i, offset, blockSize));

                offset += blockSize;
            }

            _command = command;
            _blockQueue = new ConcurrentQueue<FileMarker>(markers);
        }

        public void CalculateHash()
        {
            using (var fs = new FileStream(_command.FilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var hashGen = SHA256.Create())
                while (_blockQueue.TryDequeue(out var fileMarker))
                {

                    fs.Seek(fileMarker.Offset, SeekOrigin.Begin);
                    var buffer = new byte[fileMarker.Count];

                    fs.Read(buffer, 0, buffer.Length);

                    if (buffer.Length < 1)
                    {
                        return;
                    }

                    var hash = hashGen.ComputeHash(buffer);

                    var sb = new StringBuilder(StringHashSize);
                    foreach (var b in hash)
                    {
                        sb.Append(b.ToString("x2"));
                    }
                    Console.WriteLine($"Block #{fileMarker.Number} has hash = [{sb}]");
                }
        }
    }
}
