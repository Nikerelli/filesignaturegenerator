﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using VeeamFileSignatureGenerator.Utils;

namespace VeeamFileSignatureGenerator.Infrastructure
{
    /// <summary>
    ///     Allow to run operation paralleled depending on CPU cores count
    /// </summary>
    public class Paralleler2000 : IDisposable
    {
        private readonly ConcurrentDictionary<int,Thread> _threadPool;

        private Barrier _barrier;

        public Paralleler2000()
        {
            _threadPool = new ConcurrentDictionary<int, Thread>();
        }

        public void RunParallel(Action task)
        {
            try
            {
                var coresCount = SystemAnalyzer.GetProcessorsCount() * 2;
                _barrier = new Barrier(coresCount + 1);
                for (int i = 0; i < coresCount; i++)
                {
                    var thread = new Thread(() => GetExecutionJob(task)?.Invoke());
                    _threadPool.TryAdd(thread.ManagedThreadId, thread);
                    thread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Message: {ex.Message}\nStackTrace: {ex.StackTrace}");
                foreach (var thrd in _threadPool)
                {
                    thrd.Value.Interrupt();
                }
                _barrier?.Dispose();
            }
        }

        public void Wait()
        {
            _barrier?.SignalAndWait();
            _barrier?.Dispose();
        }

        private Action GetExecutionJob(Action job) => () =>
        {
            try
            {
                job.Invoke();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Message: {ex.Message}\nStackTrace: {ex.StackTrace}");
            }
            finally
            {
                _threadPool.TryRemove(Thread.CurrentThread.ManagedThreadId, out Thread thread);
                _barrier?.SignalAndWait();
                _barrier?.Dispose();
            }
        };

        public void Dispose()
        {
            _barrier?.Dispose();
        }
    }
}
