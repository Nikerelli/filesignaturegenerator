﻿namespace VeeamFileSignatureGenerator.Models
{
    public class BlockOfData
    {
        public int Number { get; set; }
        public byte[] Data { get; set; }
    }
}
