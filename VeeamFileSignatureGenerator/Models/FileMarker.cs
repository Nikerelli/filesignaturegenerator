﻿namespace VeeamFileSignatureGenerator.Models
{
    public struct FileMarker
    {
        public FileMarker(int number, long offset, long count)
        {
            Number = number;
            Offset = offset;
            Count = count;
        }

        public long Offset { get; }
        public long Count { get; }
        public int Number { get; }
    }
}
