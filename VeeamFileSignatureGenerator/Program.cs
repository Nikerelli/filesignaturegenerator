﻿using System;
using System.Diagnostics;
using VeeamFileSignatureGenerator.Commands;
using VeeamFileSignatureGenerator.Infrastructure;
using VeeamFileSignatureGenerator.SharedIoHashGenerator;
using VeeamFileSignatureGenerator.ThreadSafeGenerator;

namespace VeeamFileSignatureGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var command = CalculateHashCommand.Parse(args);

            AddUnhandledExceptionHandler();
            AddCancelKeyHandler();

            var timeElapsed = RunThreadSafeMode(command);

            Console.WriteLine($"Seconds passed: {timeElapsed}");

#if DEBUG
            Console.WriteLine("Hash has been calculated. Enter any key to close the app.");
            GC.Collect();
            Console.ReadKey();
#endif
        }

        /// <summary>
        ///     Best speed ratio
        /// </summary>
        private static decimal RunThreadSafeMode(CalculateHashCommand command)
        {
            var watcher = new Stopwatch();
            watcher.Start();

            var hashGenerator = new ThreadSafeHashCalculator(command);
            var threadPool = new Paralleler2000();
            threadPool.RunParallel(hashGenerator.CalculateHash);
            threadPool.Wait();

            watcher.Stop();
            return (decimal)watcher.ElapsedMilliseconds / 1000;
        }

        /// <summary>
        ///     Best memory and speed ratio for more complicated operations than simple hashing
        /// </summary>
        private static decimal RunSharedMode(CalculateHashCommand command)
        {
            var watcher = new Stopwatch();
            watcher.Start();

            var hashGenerator = new SharedHashCalculator(command);
            var threadPool = new Paralleler2000();
            threadPool.RunParallel(hashGenerator.CalculateHash);
            threadPool.Wait();
            hashGenerator?.Dispose();

            watcher.Stop();
            return (decimal)watcher.ElapsedMilliseconds / 1000;
        }

        private static void AddUnhandledExceptionHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += 
                (o, e) =>
                {
                    var exception = e.ExceptionObject as Exception;
                    Console.WriteLine($"ALARM!!!! UNHANDLED ERROR HAS BEEN DETECTED!!!\n[{nameof(exception.Message)}]: {exception?.Message}\nLittle bitch location [{nameof(exception.StackTrace)}]: {exception?.StackTrace}");
                    Console.ReadKey();
                };
        }

        private static void AddCancelKeyHandler()
        {
            Console.CancelKeyPress += (o, e) =>
            {
                Console.WriteLine("App has been interrupted");
                e.Cancel = true;
            };
        }
    }
}
