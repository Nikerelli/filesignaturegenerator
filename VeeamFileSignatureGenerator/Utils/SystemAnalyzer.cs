﻿using System.Management;

namespace VeeamFileSignatureGenerator.Utils
{
    public static class SystemAnalyzer
    {
        public static int GetProcessorsCount()
        {
            int availableCores = 0;
            foreach (var item in new ManagementObjectSearcher("Select * from Win32_Processor").Get())
            {
                availableCores += int.Parse(item["NumberOfCores"].ToString());
            }

            return availableCores;
        }
    }
}
